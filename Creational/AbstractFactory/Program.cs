﻿using System;
using Creational.AbstractFactory;

namespace Creational
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            TestAbstractFactory();
        }

        public static void TestAbstractFactory()
        {
            // to read config, this is just for testing
            Console.WriteLine("Your working env (Win or Mac):");
            var env = Console.ReadLine().ToLowerInvariant();
            Console.WriteLine($"Your env is: {env}.");

            IGUIFactory guiFactory;
            switch (env)
            {
                case "mac":
                    guiFactory = new MacFactory();
                    break;
                case "win":
                default:
                    guiFactory = new WinFactory();
                    break;
            }

            var app = new Application(guiFactory);
            app.CreateUI();
            app.Paint();
        }
    }
}
