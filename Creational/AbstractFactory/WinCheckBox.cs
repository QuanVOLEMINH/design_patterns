﻿using System;
namespace Creational.AbstractFactory
{
    public class WinCheckBox:ICheckBox
    {
        public void Paint()
        {
            Console.WriteLine("Win check box");
        }
    }
}
