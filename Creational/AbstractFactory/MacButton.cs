﻿using System;
namespace Creational.AbstractFactory
{
    public class MacButton : IButton
    {
        public MacButton()
        {
        }

        public void Paint()
        {
            Console.WriteLine("Mac button");
        }
    }
}
