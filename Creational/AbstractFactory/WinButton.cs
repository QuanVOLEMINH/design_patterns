﻿using System;

namespace Creational.AbstractFactory
{
    public class WinButton : IButton
    {
        public WinButton()
        {

        }

        public void Paint()
        {
            Console.WriteLine("Win button.");
        }
    }
}
