﻿namespace Creational.AbstractFactory
{
    public interface ICheckBox
    {
        void Paint();
    }
}
