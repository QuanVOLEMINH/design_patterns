﻿namespace Creational.AbstractFactory
{
    public interface IButton
    {
        void Paint();
    }
}
