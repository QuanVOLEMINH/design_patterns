﻿using System;
namespace Creational.AbstractFactory
{
    public class MacFactory : IGUIFactory
    {
        public MacFactory()
        {
        }

        public IButton CreateButton()
        {
            return new MacButton();
        }

        public ICheckBox CreateCheckBox()
        {
            return new MacCheckBox();
        }
    }
}