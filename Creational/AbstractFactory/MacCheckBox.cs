﻿using System;
namespace Creational.AbstractFactory
{
    public class MacCheckBox:ICheckBox
    {
        public MacCheckBox()
        {
        }

        public void Paint() {
            Console.WriteLine("Mac check box");
        }
    }
}
