﻿namespace FactoryMethod
{
    public class Gender
    {
        public string Name { get; protected init; }
    }
}
