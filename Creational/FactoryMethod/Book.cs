using System;

namespace FactoryMethod
{

    public abstract class Book
    {
        public string Name { get; protected init; }

        public abstract Gender GetGender();
    }
}