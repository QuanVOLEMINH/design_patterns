﻿namespace FactoryMethod
{
    public class HistoryBook : Book
    {
        public HistoryBook()
        {
            Name = "history";
        }

        public override Gender GetGender()
        {
            return new HistoryGender();
        }
    }
}
