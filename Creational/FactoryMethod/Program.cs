﻿using System;

namespace FactoryMethod
{
    class Program
    {
        static void Main(string[] args)
        {
            Book historyBook = new HistoryBook();
            PrintDetails(historyBook);

            Book scienceBook = new ScienceBook();
            PrintDetails(scienceBook);
        }

        static void PrintDetails(Book book)
        {
            Console.WriteLine($"Book name {book.Name} with gender {book.GetGender().Name}");
        }
    }
}
