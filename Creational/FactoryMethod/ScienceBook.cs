﻿namespace FactoryMethod
{
    public class ScienceBook : Book
    {
        public ScienceBook()
        {
            Name = "science";
        }

        public override Gender GetGender()
        {
            return new ScienceGender();
        }
    }
}
