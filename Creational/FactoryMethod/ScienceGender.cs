﻿namespace FactoryMethod
{
    public class ScienceGender : Gender
    {
        public ScienceGender()
        {
            Name = "science";
        }
    }
}
